# kr-c

## About this repo
This repo is my progress through the [C Programming Language](https://en.wikipedia.org/wiki/The_C_Programming_Language) book.

File hierarchy is laid out in the following way:

* Each chapter gets its own top level directory, then sub-sections and sub-sub-sections get their own directories as they occur.
* Exercise code files will be named as 'ex#-#.c' as they are indicated in the chapter. Any other code examples are written as they appear in the book, named in some sort of meaningful (to me) differentiation based off what the code is doing in the section/chapter as described.