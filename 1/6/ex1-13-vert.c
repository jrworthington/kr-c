#include <stdio.h>

/*
    Write a program to print histograms of the length
    of words in its input. -- veritcal version
*/

#define CHARLIMIT 15 // word char limit

int main()
{
    int carray[CHARLIMIT];
    int most, c, count, i, j;
    count = most = 0;

    for (i = 0; i < CHARLIMIT; i++) {
        carray[i] = 0;
    }

    while ((c = getchar()) != EOF) {
        if (c != ' ' && c != '\t' && c != '\n') {
            ++count;
        }
        if ((c == ' ' || c == '\t' || c == '\n') && count != 0) {
            carray[count] = carray[count] + 1;
            count = 0;
        }
    }

    printf("\n");

    for(i = 0; i < CHARLIMIT; i++) {
	    if (carray[i] > most) {
		    most = carray[i];
	    }
    }

    for (i = most; i >= 1; i--) {
	    for (j = 1; j < CHARLIMIT; j++) {
		    if (carray[j] >= i) {
			    printf(" x\t");
		    } else {
			    printf("  \t");
		    }
	    }
	    printf("\n");
    }

    for (i = 1; i < CHARLIMIT; i++) {
	    printf("%2d\t", i);
    }
    printf("\n");

    return 0;
}
