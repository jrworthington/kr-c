#include <stdio.h>

/*
    Write a program to print histograms of the length
    of words in its input. -- Horizontal version
*/

#define CHARLIMIT 15 // word char limit

int main()
{
    int carray[CHARLIMIT];
    int c, count, i, j;
    count = 0;

    for (i = 0; i < CHARLIMIT; i ++) {
        carray[i] = 0;
    }

    while ((c = getchar()) != EOF) {
        if (c != ' ' && c != '\t' && c != '\n') {
            ++count;
        }
        if ((c == ' ' || c == '\t' || c == '\n') && count != 0) {
            carray[count] = carray[count] + 1;
            count = 0;
        }
    }

    printf("\n");

    for (i = 1; i < CHARLIMIT; i++) {
        printf("%2d\t", i);
        for (j = 1; j <= carray[i]; j++) {
            printf("x");
        }
        printf("\n");
    }

    return 0;
}