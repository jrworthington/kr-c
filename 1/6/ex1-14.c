#include <stdio.h>

/*
    Write a program to print histograms of the frequencies 
    of different characters in its input
*/

#define LETTERS 26

int main()
{
    int carray[LETTERS];
    int c, most, count, i, j;
    most = count = 0;

    for (i = 0; i < LETTERS; i++) {
        carray[i] = 0;
    }

    while ((c = getchar()) != EOF) {
        if (c >= 'A' && c <= 'z') {
           if(c >= 'A' && c <= 'Z') {
            carray[c - 'A']++;
           } 
           if (c >= 'a' && c <= 'z') {
            carray[c - 'a']++;
           }
        }
    }

    printf("\n");

    for(i = 0; i < LETTERS; i++) {
	    if (carray[i] > most) {
		    most = carray[i];
	    }
    }

    for (i = most; i >= 1; i--) {
	    for (j = 0; j < LETTERS; j++) {
		    if (carray[j] >= i) {
			    printf("x|");
		    } else {
			    printf(" |");
		    }
	    }
	    printf("\n");
    }

    for (i = 0; i < LETTERS; i++) {
        printf("==");
    }
    printf("\n");

    for (i = 0; i < LETTERS; i++) {
        if (i + 'A' <= 'Z'){
            printf("%c|", i + 'A');
        }
    }
    printf("\n");

    return 0;
}
