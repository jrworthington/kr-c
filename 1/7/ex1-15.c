#include <stdio.h>

/*
    Rewrite the temperature conversion program of Section 1.2
    to use a function for conversion
*/

/* print Fahrenheit-Celsius table
    for fahr = 0, 20, ..., 300 */

int conversion(int fahr);

int main() 
{
    int fahr, celsius;
    int lower, upper, step;

    lower = 0;      /* lower limit of the temperature scale */
    upper = 300;    /* upper limit */
    step = 20;      /* step size */

    fahr = lower;
    while (fahr <= upper ) {
        celsius = conversion(fahr);
        printf("%d\t%d\n", fahr, celsius);
        fahr = fahr + step;
    }
}

int conversion(int fahr)
{
    return 5 * (fahr - 32) / 9;
}