#include <stdio.h>
#define MAXLINE 10    /* maximum input line length */

int lgetline(char line[], int maxline);
void copy(char to[], char from[]);

/*
    Revise the main routine of the longest-line program
    so it will correctly print the length of arbitrary
    long input lines, and as much as possible of the text
*/

/* print the longest input line */

int main()
{
    int len;            /* current line length */
    int max;            /* maximium length seen so far */
    char line[MAXLINE];     /* current input line */
    char longest[MAXLINE];  /* longest line saved here */

    max = 0;
    while ((len = lgetline(line, MAXLINE)) > 0) {
        if (len > max) {
            max = len;
            copy(longest, line);
        }
    }

    if (max > 0) {  /* there was a line */
        printf("%d %s", max, longest);
    }

    return 0;
}

/* lgetline: read a line into s, return length */
int lgetline(char s[], int lim)
{
    int c, i;

    for (i = 0; (c = getchar()) != EOF && c != '\n'; ++i) {
        if (i < lim - 1) {
            s[i] = c;
        }
    }
    if (i < lim - 1 && c == '\n') {
        s[i] =c;
        ++i;
        s[i] = '\0';
    }
    if (i >= lim - 1) {
        s[lim - 1] = '\0';
        ++i;
    }
    return i;
}

/* copy: copy 'from' into 'to'; assume to is big enough */
void copy(char to[], char from[])
{
    int i;

    i = 0;
    while ((to[i] = from[i]) != '\0' && i < MAXLINE-1) {
        ++i;
    }
}