#include <stdio.h>

/* Write a program to copy its input to its output, 
     replacing each string of one or more blanks by
     a single blank. */
int main()
{
    int c, oc;

    while ((c = getchar()) != EOF) {
        if (c != ' ') {
            putchar(c);
        } else if (oc != ' ') {
            putchar(c);
        }
        oc = c;
    }

    return 0;
}