#include <stdio.h>

/* write a program to count blanks, tabs, and newlines */
int main()
{
    int cblank = 0;
    int ctab = 0;
    int cnl = 0;
    int c;
    
    while ((c = getchar()) != EOF) {
        if (c == ' ')
            ++cblank;
        if (c == '\t')
            ++ctab;
        if (c == '\n')
            ++cnl;
    }

    printf("%d %d %d\n", cblank, ctab, cnl);

    return 0;
}