#include <stdio.h>

/* verify that the expression getchar() != EOF is 0 or 1 */

int main()
{
    int c;

    while(c = getchar() != EOF) 
        printf("%d\n", c);

    printf("%d\n", c);
    return 0;
}