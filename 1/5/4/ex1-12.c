#include <stdio.h>

/* 
    Write a program that prints its input one word per line.
*/
int main()
{
    int c, oc;
    while ((c = getchar()) != EOF){
        if ((c == ' ' || c == '\t') && (oc != ' ' &&  oc != '\t')) {
            putchar('\n');
        } else if ((c == ' ' || c == '\t') && (oc == ' ' || oc == '\t')) {
            ;
        } else {
            putchar(c);
        }
        oc = c;
    }

    return 0;
}